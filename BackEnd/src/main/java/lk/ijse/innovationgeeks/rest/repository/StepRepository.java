package lk.ijse.innovationgeeks.rest.repository;

import lk.ijse.innovationgeeks.rest.entity.Step;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StepRepository extends JpaRepository<Step, Integer> {
}
