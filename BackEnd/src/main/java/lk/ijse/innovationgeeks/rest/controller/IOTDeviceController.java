package lk.ijse.innovationgeeks.rest.controller;

import lk.ijse.innovationgeeks.rest.dto.IOTDeviceDTO;
import lk.ijse.innovationgeeks.rest.service.IOTDeviceService;
import lk.ijse.innovationgeeks.rest.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * IOTDevice Controller(Severside endpoint)
 */

@RestController
@CrossOrigin
@RequestMapping("api/v1/iotDevices")
public class IOTDeviceController {
    @Autowired
    private IOTDeviceService iotDeviceService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String connect() {

        try {
            iotDeviceService.newDeviceRegister(new IOTDeviceDTO());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Working";
    }
}
