package lk.ijse.innovationgeeks.rest.dto;

public class StepDTO {

    private int stepNumber;
    private boolean itemStatus;
    private LineDTO lineDTO;
    private IOTDeviceDTO iotDeviceDTO;


    public StepDTO() {
    }

    public StepDTO(int stepNumber, boolean itemStatus, LineDTO lineDTO, IOTDeviceDTO iotDeviceDTO) {
        this.setStepNumber(stepNumber);
        this.setItemStatus(itemStatus);
        this.setLineDTO(lineDTO);
        this.setIotDeviceDTO(iotDeviceDTO);
    }


    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public boolean itemStatus() {
        return itemStatus;
    }

    public void setItemStatus(boolean itemStatus) {
        this.itemStatus = itemStatus;
    }

    public LineDTO getLineDTO() {
        return lineDTO;
    }

    public void setLineDTO(LineDTO lineDTO) {
        this.lineDTO = lineDTO;
    }

    public IOTDeviceDTO getIotDeviceDTO() {
        return iotDeviceDTO;
    }

    public void setIotDeviceDTO(IOTDeviceDTO iotDeviceDTO) {
        this.iotDeviceDTO = iotDeviceDTO;
    }

}
