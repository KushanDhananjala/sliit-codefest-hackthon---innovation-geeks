package lk.ijse.innovationgeeks.rest;

import lk.ijse.innovationgeeks.rest.service.FolderWatcherService;
import lk.ijse.innovationgeeks.rest.service.impl.FolderWatcherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class main extends SpringBootServletInitializer {

    /**
     * create object of FolderWatcherServiceImpl
     */
//    @Autowired
//    private FolderWatcherService folderWatcherService;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(main.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(main.class, args);
        new FolderWatcherServiceImpl().shiftDataFolderWatcher();
    }

//    @PostConstruct
//    public void init() {
//        /**
//         * call folder watcher method to watch csv files folder
//         */
//        folderWatcherService.shiftDataFolderWatcher();
//    }
}
