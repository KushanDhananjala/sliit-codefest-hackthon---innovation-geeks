package lk.ijse.innovationgeeks.rest.controller;

import lk.ijse.innovationgeeks.rest.service.FolderWatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("")
public class ShiftManagementController {

    @Autowired
    private FolderWatcherService folderWatcherService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public void shiftManage() {
        folderWatcherService.shiftDataFolderWatcher();
    }
}