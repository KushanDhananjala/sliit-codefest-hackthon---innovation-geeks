package lk.ijse.innovationgeeks.rest.service;


import lk.ijse.innovationgeeks.rest.dto.LineDTO;

public interface LineService {

    boolean registerNewLine(LineDTO lineDTO)throws Exception;
}
