package lk.ijse.innovationgeeks.rest.dto;

public class IOTDeviceDTO {

    private String deviceID;
    private String lineID;
    private String deviceName;

    public IOTDeviceDTO() {
    }

    public IOTDeviceDTO(String deviceID, String lineID, String deviceName) {
        this.setDeviceID(deviceID);
        this.setLineID(lineID);
        this.setDeviceName(deviceName);
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
