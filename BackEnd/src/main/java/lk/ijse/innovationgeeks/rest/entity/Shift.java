package lk.ijse.innovationgeeks.rest.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
public class Shift {

    @Id
    private String shiftID;
    private String shiftDate;
    private String startTime;
    private String endTime;
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "managerID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Manager manager;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<ShiftLineDetail> shiftLineDetails;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<ShiftEmployeeDetail> shiftEmployeeDetails;

    public Shift() {
    }

    public Shift(String shiftID, String shiftDate, String startTime, String endTime, Manager manager, List<ShiftLineDetail> shiftLineDetails, List<ShiftEmployeeDetail> shiftEmployeeDetails) {
        this.shiftID = shiftID;
        this.shiftDate = shiftDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.manager = manager;
        this.shiftLineDetails = shiftLineDetails;
        this.shiftEmployeeDetails = shiftEmployeeDetails;
    }

    public String getShiftID() {
        return shiftID;
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }

    public String getShiftDate() {
        return shiftDate;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = shiftDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public List<ShiftLineDetail> getShiftLineDetails() {
        return shiftLineDetails;
    }

    public void setShiftLineDetails(List<ShiftLineDetail> shiftLineDetails) {
        this.shiftLineDetails = shiftLineDetails;
    }

    public List<ShiftEmployeeDetail> getShiftEmployeeDetails() {
        return shiftEmployeeDetails;
    }

    public void setShiftEmployeeDetails(List<ShiftEmployeeDetail> shiftEmployeeDetails) {
        this.shiftEmployeeDetails = shiftEmployeeDetails;
    }

    @Override
    public String toString() {
        return "Shift{" +
                "shiftID='" + shiftID + '\'' +
                ", shiftDate='" + shiftDate + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", manager=" + manager +
                ", shiftLineDetails=" + shiftLineDetails +
                ", shiftEmployeeDetails=" + shiftEmployeeDetails +
                '}';
    }
}
