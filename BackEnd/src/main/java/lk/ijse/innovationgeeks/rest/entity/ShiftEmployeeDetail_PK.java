package lk.ijse.innovationgeeks.rest.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ShiftEmployeeDetail_PK implements Serializable {

    private String shiftID;
    private String employeeID;

    public ShiftEmployeeDetail_PK() {
    }

    public ShiftEmployeeDetail_PK(String shiftID, String employeeID) {
        this.shiftID = shiftID;
        this.employeeID = employeeID;
    }
}
