package lk.ijse.innovationgeeks.rest.service.impl;

import lk.ijse.innovationgeeks.rest.dto.IOTDeviceDTO;
import lk.ijse.innovationgeeks.rest.repository.IOTDeviceRepository;
import lk.ijse.innovationgeeks.rest.repository.ShiftRepository;
import lk.ijse.innovationgeeks.rest.service.IOTDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class IOTDeviceServiceImpl implements IOTDeviceService {

    @Autowired
    private ShiftRepository iotDeviceRepository;

    /**
     * newDeviceRegister() for add new Device
     * @param dto
     * @return
     * @throws Exception
     */
    @Override
    public boolean newDeviceRegister(IOTDeviceDTO dto) throws Exception {
        System.out.println("Autowired Working: "+iotDeviceRepository);
        return false;
    }
}
