package lk.ijse.innovationgeeks.rest.service;

import lk.ijse.innovationgeeks.rest.dto.StepDTO;
import lk.ijse.innovationgeeks.rest.dto.StepResultDTO;

public interface StepService {


    public StepResultDTO stepI(StepDTO stepDTO) throws Exception;

    public StepResultDTO stepII(StepDTO stepDTO) throws Exception;

    public StepResultDTO stepIII(StepDTO stepDTO) throws Exception;

    public StepResultDTO stepIV(StepDTO stepDTO) throws Exception;

    public StepResultDTO stepV(StepDTO stepDTO) throws Exception;

    public StepResultDTO stepIAvgRatePerFiveMinutes() throws Exception;

    public StepResultDTO stepIIAvgRatePerFiveMinutes() throws Exception;

    public StepResultDTO stepIIIAvgRatePerFiveMinutes() throws Exception;

    public StepResultDTO stepIVAvgRatePerFiveMinutes() throws Exception;

    public StepResultDTO stepVAvgRatePerFiveMinutes() throws Exception;
}
