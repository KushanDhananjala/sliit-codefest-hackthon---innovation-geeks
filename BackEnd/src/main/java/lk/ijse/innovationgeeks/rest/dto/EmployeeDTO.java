package lk.ijse.innovationgeeks.rest.dto;

public class EmployeeDTO {

    private String employeeID;
    private String lineID;
    private String managerID;
    private String employeeName;
    private String workingSection;

    public EmployeeDTO() {
    }

    public EmployeeDTO(String employeeID, String lineID, String managerID, String employeeName, String workingSection) {
        this.setEmployeeID(employeeID);
        this.setLineID(lineID);
        this.setManagerID(managerID);
        this.setEmployeeName(employeeName);
        this.setWorkingSection(workingSection);
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getManagerID() {
        return managerID;
    }

    public void setManagerID(String managerID) {
        this.managerID = managerID;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getWorkingSection() {
        return workingSection;
    }

    public void setWorkingSection(String workingSection) {
        this.workingSection = workingSection;
    }
}
