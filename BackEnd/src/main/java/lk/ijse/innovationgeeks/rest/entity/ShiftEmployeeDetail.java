package lk.ijse.innovationgeeks.rest.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ShiftEmployeeDetail {

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Shift shift;
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Employee employee;
    @EmbeddedId
    private ShiftEmployeeDetail_PK shiftEmployeeDetail_pk;

    public ShiftEmployeeDetail() {
    }

    public ShiftEmployeeDetail(Shift shift, Employee employee, String shiftID, String employeeID) {
        this.shift = shift;
        this.employee = employee;
        this.shiftEmployeeDetail_pk = new ShiftEmployeeDetail_PK(shiftID, employeeID);
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public ShiftEmployeeDetail_PK getShiftEmployeeDetail_pk() {
        return shiftEmployeeDetail_pk;
    }

    public void setShiftEmployeeDetail_pk(ShiftEmployeeDetail_PK shiftEmployeeDetail_pk) {
        this.shiftEmployeeDetail_pk = shiftEmployeeDetail_pk;
    }

    @Override
    public String toString() {
        return "ShiftEmployeeDetail{" +
                "shift=" + shift +
                ", employee=" + employee +
                ", shiftEmployeeDetail_pk=" + shiftEmployeeDetail_pk +
                '}';
    }
}
