package lk.ijse.innovationgeeks.rest.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ShiftLineDetail {

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Shift shift;
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Line line;
    @EmbeddedId
    private ShiftLineDetail_PK shiftLineDetail_pk;

    public ShiftLineDetail() {
    }

    public ShiftLineDetail(Shift shift, Line line, String shiftID, String lineID) {
        this.setShift(shift);
        this.setLine(line);
        this.shiftLineDetail_pk = new ShiftLineDetail_PK(shiftID, lineID);
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public ShiftLineDetail_PK getShiftLineDetail_pk() {
        return shiftLineDetail_pk;
    }

    public void setShiftLineDetail_pk(ShiftLineDetail_PK shiftLineDetail_pk) {
        this.shiftLineDetail_pk = shiftLineDetail_pk;
    }

    @Override
    public String toString() {
        return "ShiftLineDetail{" +
                "shift=" + shift +
                ", line=" + line +
                ", shiftLineDetail_pk=" + shiftLineDetail_pk +
                '}';
    }
}
