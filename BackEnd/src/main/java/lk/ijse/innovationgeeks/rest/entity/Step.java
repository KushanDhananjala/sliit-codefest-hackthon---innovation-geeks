package lk.ijse.innovationgeeks.rest.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Step implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long stepID;
    private int stepNumber;
    private boolean itemStatus;
    @OneToOne(cascade = CascadeType.MERGE)
    private Line line;
    @OneToOne(cascade = CascadeType.MERGE)
    private IOTDevice iotDevice;


    public Step() {
    }

    public Step(int stepNumber, boolean itemStatus, Line line, IOTDevice iotDevice) {
        this.stepNumber = stepNumber;
        this.itemStatus = itemStatus;
        this.line = line;
        this.iotDevice = iotDevice;
    }

    public long getStepID() {
        return stepID;
    }

    public void setStepID(long stepID) {
        this.stepID = stepID;
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public boolean isItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(boolean itemStatus) {
        this.itemStatus = itemStatus;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public IOTDevice getIotDevice() {
        return iotDevice;
    }

    public void setIotDevice(IOTDevice iotDevice) {
        this.iotDevice = iotDevice;
    }

    @Override
    public String toString() {
        return "Step{" +
                "stepID=" + stepID +
                ", stepNumber=" + stepNumber +
                ", itemStatus=" + itemStatus +
                ", line=" + line +
                ", iotDevice=" + iotDevice +
                '}';
    }
}
