package lk.ijse.innovationgeeks.rest.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Employee {

    @Id
    private String employeeID;
    @ManyToOne
    @JoinColumn(name = "lineID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Line line;
    @ManyToOne
    @JoinColumn(name = "managerID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Manager manager;
    private String employeeName;
    private String workingSection;

    public Employee() {
    }

    public Employee(String employeeID, Line line, Manager manager, String employeeName, String workingSection) {
        this.employeeID = employeeID;
        this.line = line;
        this.manager = manager;
        this.employeeName = employeeName;
        this.workingSection = workingSection;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getWorkingSection() {
        return workingSection;
    }

    public void setWorkingSection(String workingSection) {
        this.workingSection = workingSection;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeID='" + employeeID + '\'' +
                ", line=" + line +
                ", manager=" + manager +
                ", employeeName='" + employeeName + '\'' +
                ", workingSection='" + workingSection + '\'' +
                '}';
    }
}
