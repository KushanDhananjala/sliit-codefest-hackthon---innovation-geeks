package lk.ijse.innovationgeeks.rest.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ShiftLineDetail_PK implements Serializable {

    private String shiftID;
    private String lineID;

    public ShiftLineDetail_PK() {
    }

    public ShiftLineDetail_PK(String shiftID, String lineID) {
        this.shiftID = shiftID;
        this.lineID = lineID;
    }
}
