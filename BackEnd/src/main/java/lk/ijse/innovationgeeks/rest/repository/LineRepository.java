package lk.ijse.innovationgeeks.rest.repository;

import lk.ijse.innovationgeeks.rest.entity.Line;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LineRepository extends CrudRepository<Line, String> {

}
