package lk.ijse.innovationgeeks.rest.dto;

public class ManagerDTO {

    private String managerID;
    private String managerName;

    public ManagerDTO() {
    }

    public ManagerDTO(String managerID, String managerName) {
        this.setManagerID(managerID);
        this.setManagerName(managerName);
    }

    public String getManagerID() {
        return managerID;
    }

    public void setManagerID(String managerID) {
        this.managerID = managerID;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
}
