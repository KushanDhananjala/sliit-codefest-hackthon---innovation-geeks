package lk.ijse.innovationgeeks.rest.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Manager {

    @Id
    private
    String managerID;
    private String managerName;

    public Manager() {
    }

    public Manager(String managerID, String managerName) {
        this.setManagerID(managerID);
        this.setManagerName(managerName);
    }

    public String getManagerID() {
        return managerID;
    }

    public void setManagerID(String managerID) {
        this.managerID = managerID;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "managerID='" + managerID + '\'' +
                ", managerName='" + managerName + '\'' +
                '}';
    }
}
