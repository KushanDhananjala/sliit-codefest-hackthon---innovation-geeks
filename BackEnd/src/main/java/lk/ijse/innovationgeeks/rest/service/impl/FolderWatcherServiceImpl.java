package lk.ijse.innovationgeeks.rest.service.impl;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lk.ijse.innovationgeeks.rest.dto.ShiftDTO;
import lk.ijse.innovationgeeks.rest.dto.ShiftEmployeeDetailDTO;
import lk.ijse.innovationgeeks.rest.dto.ShiftLineDetailDTO;
import lk.ijse.innovationgeeks.rest.entity.*;
import lk.ijse.innovationgeeks.rest.repository.LineRepository;
import lk.ijse.innovationgeeks.rest.repository.ManagerReposiory;
import lk.ijse.innovationgeeks.rest.repository.ShiftRepository;
import lk.ijse.innovationgeeks.rest.service.FolderWatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.*;


@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FolderWatcherServiceImpl implements FolderWatcherService {

    @Autowired
    private ShiftRepository shiftRepository;
    @Autowired
    private ManagerReposiory managerReposiory;
    @Autowired
    private LineRepository lineRepository;
    @Autowired
    private ShiftRepository getShiftRepository;

    /**
     * shiftDataFolderWatcher() method for listening folder changes. And pass csv file to read method
     */
    @Override
    public void shiftDataFolderWatcher() {
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            Path dir = Paths.get("D:/CSV FIle Locator/");
            dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

            System.out.println("Watch Service listening for " + dir.getFileName());

            while (true) {
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException ex) {
                    return;
                }

                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();

                    if (kind == ENTRY_MODIFY) {
                        @SuppressWarnings("unchecked")
                        WatchEvent<Path> ev = (WatchEvent<Path>) event;
                        Path fileName = ev.context();


                        readFile(Paths.get("D:/CSV FIle Locator/" + fileName));

                        System.out.println(kind.name() + ": " + fileName);
                    }
                }

                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }

        } catch (FileSystemException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }


    /**
     * readFile() for Read csv file and persists data to database
     *
     * @param path
     */
    public void readFile(Path path) {
        Reader readerEmployee = null;

        ShiftDTO shiftDTO = new ShiftDTO();

        List<ShiftEmployeeDetailDTO> shiftEmployeeDetailDTOS = new ArrayList<>();
        List<ShiftLineDetailDTO> shiftLineDetailDTOS = new ArrayList<>();

        try {
            readerEmployee = Files.newBufferedReader(path);

            CSVReader csvReader = new CSVReaderBuilder(readerEmployee).withSkipLines(1).build();

            String[] nextRecord;
            while ((nextRecord = csvReader.readNext()) != null) {

                shiftDTO.setShiftID(nextRecord[0]);
                shiftDTO.setManagerID(nextRecord[4]);
                shiftDTO.setStartTime(nextRecord[2]);
                shiftDTO.setEndTime(nextRecord[3]);
                shiftDTO.setShiftDate(nextRecord[1]);

                ShiftEmployeeDetailDTO shiftEmployeeDetailDTO =
                        new ShiftEmployeeDetailDTO(nextRecord[0], nextRecord[5], nextRecord[6]);

                ShiftLineDetailDTO shiftLineDetailDTO = new ShiftLineDetailDTO(
                        nextRecord[0],
                        nextRecord[5]
                );

                shiftEmployeeDetailDTOS.add(shiftEmployeeDetailDTO);
                shiftLineDetailDTOS.add(shiftLineDetailDTO);
            }

            shiftDTO.setShiftEmployeeDetailDTOs(shiftEmployeeDetailDTOS);
            Shift shift = new Shift();
            shift.setShiftID(shiftDTO.getShiftID());
            shift.setEndTime(shiftDTO.getEndTime());
            shift.setShiftDate(shiftDTO.getShiftDate());
            shift.setStartTime(shiftDTO.getStartTime());

            Manager manager = managerReposiory.findById(shiftDTO.getManagerID()).get();
            shift.setManager(manager);

            List<ShiftEmployeeDetail> shiftEmployeeDetails = new ArrayList<>();

            for (ShiftEmployeeDetailDTO detailDTO : shiftEmployeeDetailDTOS) {
                Employee employee = new Employee();
                employee.setEmployeeID(detailDTO.getEmployeeID());
                ShiftEmployeeDetail_PK shiftEmployeeDetail_pk = new ShiftEmployeeDetail_PK(
                        shiftDTO.getShiftID(),
                        detailDTO.getEmployeeID()
                );
                ShiftEmployeeDetail shiftEmployeeDetail = new ShiftEmployeeDetail(
                        shift,
                        employee,
                        detailDTO.getShiftID(),
                        detailDTO.getEmployeeID()
                );
                shiftEmployeeDetails.add(shiftEmployeeDetail);
            }
            List<ShiftLineDetail> shiftLineDetails = new ArrayList<>();

            for (ShiftLineDetailDTO detailDTO : shiftLineDetailDTOS) {
                Line line = lineRepository.findById(detailDTO.getLineID()).get();
                ShiftLineDetail_PK shiftLineDetail_pk = new ShiftLineDetail_PK(
                        detailDTO.getShiftID(),
                        detailDTO.getLineID()
                );
                ShiftLineDetail shiftLineDetail = new ShiftLineDetail(
                        shift,
                        line,
                        detailDTO.getShiftID(),
                        detailDTO.getLineID()
                );

                shiftLineDetails.add(shiftLineDetail);
            }

            shift.setShiftEmployeeDetails(shiftEmployeeDetails);
            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
//            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" + shiftEmployeeDetails == null);
//            shift.setShiftLineDetails(shiftLineDetails);xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            shiftRepository.save(shift);

            readerEmployee.close();
            csvReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
