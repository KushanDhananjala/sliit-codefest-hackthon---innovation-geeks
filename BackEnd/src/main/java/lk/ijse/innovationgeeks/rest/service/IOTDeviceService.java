package lk.ijse.innovationgeeks.rest.service;

import lk.ijse.innovationgeeks.rest.dto.IOTDeviceDTO;

public interface IOTDeviceService {

    public boolean newDeviceRegister(IOTDeviceDTO dto)throws Exception;
}
