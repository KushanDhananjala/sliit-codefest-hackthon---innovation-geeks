package lk.ijse.innovationgeeks.rest.controller;

import lk.ijse.innovationgeeks.rest.dto.StepDTO;
import lk.ijse.innovationgeeks.rest.dto.StepResultDTO;
import lk.ijse.innovationgeeks.rest.service.FolderWatcherService;
import lk.ijse.innovationgeeks.rest.service.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/stepI")
public class StepIController {
    @Autowired
    private StepService stepService;
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public StepResultDTO stepI(@RequestBody StepDTO stepDTO) {
        try {
            return stepService.stepI(stepDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public StepResultDTO stepIAvgRatePerFiveMinutes() {
        try {
            return stepService.stepIAvgRatePerFiveMinutes();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
