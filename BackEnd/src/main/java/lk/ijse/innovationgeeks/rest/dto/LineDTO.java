package lk.ijse.innovationgeeks.rest.dto;

import java.util.List;

public class LineDTO {

    private String lineID;
    private String lineName;
    private List<IOTDeviceDTO> iotDeviceDTOS;

    public LineDTO() {
    }

    public LineDTO(String lineID, String lineName) {
        this.setLineID(lineID);
        this.setLineName(lineName);
    }

    public LineDTO(String lineID, String lineName, List<IOTDeviceDTO> iotDeviceDTOS) {
        this.lineID = lineID;
        this.lineName = lineName;
        this.setIotDeviceDTOS(iotDeviceDTOS);
    }

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }


    public List<IOTDeviceDTO> getIotDeviceDTOS() {
        return iotDeviceDTOS;
    }

    public void setIotDeviceDTOS(List<IOTDeviceDTO> iotDeviceDTOS) {
        this.iotDeviceDTOS = iotDeviceDTOS;
    }
}
