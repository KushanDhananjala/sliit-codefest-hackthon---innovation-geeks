package lk.ijse.innovationgeeks.rest.repository;

import lk.ijse.innovationgeeks.rest.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerReposiory extends JpaRepository<Manager, String> {
}
