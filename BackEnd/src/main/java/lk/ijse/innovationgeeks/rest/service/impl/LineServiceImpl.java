package lk.ijse.innovationgeeks.rest.service.impl;

import lk.ijse.innovationgeeks.rest.dto.IOTDeviceDTO;
import lk.ijse.innovationgeeks.rest.dto.LineDTO;
import lk.ijse.innovationgeeks.rest.entity.IOTDevice;
import lk.ijse.innovationgeeks.rest.entity.Line;
import lk.ijse.innovationgeeks.rest.repository.LineRepository;
import lk.ijse.innovationgeeks.rest.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LineServiceImpl implements LineService {
    @Autowired
    private LineRepository lineRepository;

    @Override
    public boolean registerNewLine(LineDTO lineDTO) throws Exception {
        Line line = new Line();
        line.setLineID(lineDTO.getLineID());
        List<IOTDeviceDTO> iotDeviceDTOS = lineDTO.getIotDeviceDTOS();
        List<IOTDevice> iotDevices = new ArrayList<>();
        for (IOTDeviceDTO iotDeviceDTO : iotDeviceDTOS) {

            IOTDevice iotDevice = new IOTDevice(
                    iotDeviceDTO.getDeviceID(),
                    line,
                    iotDeviceDTO.getDeviceName()
            );
            iotDevices.add(iotDevice);

        }
        line.setLineName(lineDTO.getLineName());
        line.setIotDevices(iotDevices);
        lineRepository.save(line);
        return true;
    }
}
