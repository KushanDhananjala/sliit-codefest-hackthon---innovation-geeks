package lk.ijse.innovationgeeks.rest.dto;

public class ShiftEmployeeDetailDTO {

    private String shiftID;
    private String lineID;
    private String employeeID;

    public ShiftEmployeeDetailDTO() {
    }

    public ShiftEmployeeDetailDTO(String shiftID, String lineID, String employeeID) {
        this.setShiftID(shiftID);
        this.setLineID(lineID);
        this.setEmployeeID(employeeID);
    }

    public String getShiftID() {
        return shiftID;
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }
}
