package lk.ijse.innovationgeeks.rest.service.impl;

import lk.ijse.innovationgeeks.rest.dto.StepDTO;
import lk.ijse.innovationgeeks.rest.dto.StepResultDTO;
import lk.ijse.innovationgeeks.rest.service.StepService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SetpServiceImpl implements StepService {

    private int stepITotalRequestCount = 0;
    private int stepIStatusFalseCount = 0;
    private int stepIStatusTrueCount = 0;
    private int stepIErrorPrecentage = 0;
    private int stepILastFiveMinutesRequestsCount = 0;

    private int stepIITotalRequestCount = 0;
    private int stepIIStatusFalseCount = 0;
    private int stepIIStatusTrueCount = 0;
    private int stepIIErrorPrecentage = 0;
    private int stepIILastFiveMinutesRequestsCount = 0;

    private int stepIIITotalRequestCount = 0;
    private int stepIIIStatusFalseCount = 0;
    private int stepIIIStatusTrueCount = 0;
    private int stepIIIErrorPrecentage = 0;
    private int stepIIILastFiveMinutesRequestsCount = 0;

    private int stepIVTotalRequestCount = 0;
    private int stepIVStatusFalseCount = 0;
    private int stepIVStatusTrueCount = 0;
    private int stepIVErrorPrecentage = 0;
    private int stepIVLastFiveMinutesRequestsCount = 0;

    private int stepVTotalRequestCount = 0;
    private int stepVStatusFalseCount = 0;
    private int stepVStatusTrueCount = 0;
    private int stepVErrorPrecentage = 0;
    private int stepVLastFiveMinutesRequestsCount = 0;

    /**
     *Read data received by IOT devices as Http requests and process them to show chart and save data to database
     * Step by Step
     * @param stepDTO
     * @return
     * @throws Exception
     */

    @Override
    public StepResultDTO stepI(StepDTO stepDTO) throws Exception {

        stepITotalRequestCount++;
        stepILastFiveMinutesRequestsCount++;

        if (stepDTO.itemStatus() == true) {
            stepIStatusTrueCount++;
        }

        stepIStatusFalseCount = stepITotalRequestCount - stepIStatusTrueCount;
        stepIErrorPrecentage = (int) ((stepIStatusFalseCount * 100.0f) / stepITotalRequestCount);

        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(stepDTO.getStepNumber());
        stepResultDTO.setErrorPercentage(stepIErrorPrecentage);
        stepResultDTO.setTime(new SimpleDateFormat("ss").format(new Date()));

        return stepResultDTO;
    }

    /**
     *Read data received by IOT devices as Http requests and process them to show chart and save data to database
     * Step by Step
     * @param stepDTO
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepII(StepDTO stepDTO) throws Exception {

        stepIITotalRequestCount++;
        stepIILastFiveMinutesRequestsCount++;

        if (stepDTO.itemStatus() == true) {
            stepIIStatusTrueCount++;
        }

        stepIIStatusFalseCount = stepIITotalRequestCount - stepIIStatusTrueCount;
        stepIIErrorPrecentage = (int) ((stepIIStatusFalseCount * 100.0f) / stepIITotalRequestCount);

        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(stepDTO.getStepNumber());
        stepResultDTO.setErrorPercentage(stepIIErrorPrecentage);

        return stepResultDTO;
    }

    /**
     *Read data received by IOT devices as Http requests and process them to show chart and save data to database
     * Step by Step
     * @param stepDTO
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepIII(StepDTO stepDTO) throws Exception {

        stepIIITotalRequestCount++;
        stepIIILastFiveMinutesRequestsCount++;

        if (stepDTO.itemStatus() == true) {
            stepIIIStatusTrueCount++;
        }

        stepIIIStatusFalseCount = stepIIITotalRequestCount - stepIIIStatusTrueCount;
        stepIIIErrorPrecentage = (int) ((stepIIIStatusFalseCount * 100.0f) / stepIIITotalRequestCount);

        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(stepDTO.getStepNumber());
        stepResultDTO.setErrorPercentage(stepIIIErrorPrecentage);

        return stepResultDTO;
    }

    /**
     *Read data received by IOT devices as Http requests and process them to show chart and save data to database
     * Step by Step
     * @param stepDTO
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepIV(StepDTO stepDTO) throws Exception {

        stepIVTotalRequestCount++;
        stepIVLastFiveMinutesRequestsCount++;

        if (stepDTO.itemStatus() == true) {
            stepIVStatusTrueCount++;
        }

        stepIVStatusFalseCount = stepIVTotalRequestCount - stepIVStatusTrueCount;
        stepIVErrorPrecentage = (int) ((stepIVStatusFalseCount * 100.0f) / stepIVTotalRequestCount);

        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(stepDTO.getStepNumber());
        stepResultDTO.setErrorPercentage(stepIVErrorPrecentage);

        return stepResultDTO;
    }

    /**
     *Read data received by IOT devices as Http requests and process them to show chart and save data to database
     * Step by Step
     * @param stepDTO
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepV(StepDTO stepDTO) throws Exception {

        stepVTotalRequestCount++;
        stepVLastFiveMinutesRequestsCount++;

        if (stepDTO.itemStatus() == true) {
            stepVStatusTrueCount++;
        }

        stepVStatusFalseCount = stepVTotalRequestCount - stepVStatusTrueCount;
        stepVErrorPrecentage = (int) ((stepVStatusFalseCount * 100.0f) / stepVTotalRequestCount);

        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(stepDTO.getStepNumber());
        stepResultDTO.setErrorPercentage(stepVErrorPrecentage);

        return stepResultDTO;
    }

    /**
     * average items per second for last 5 minutes step1
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepIAvgRatePerFiveMinutes() throws Exception {
        int avgRateOfLastFiveMin = stepILastFiveMinutesRequestsCount / (5 * 60);
        stepILastFiveMinutesRequestsCount = 0;

        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(1);
        stepResultDTO.setProcessingRate(avgRateOfLastFiveMin);
        stepResultDTO.setTime(new SimpleDateFormat("ss").format(new Date()));
//        stepResultDTO.setTime(Integer.toString(calendar.get(Calendar.SECOND)));

        return stepResultDTO;
    }

    /**
     * average items per second for last 5 minutes step2
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepIIAvgRatePerFiveMinutes() throws Exception {
        int avgRateOfLastFiveMin = stepIILastFiveMinutesRequestsCount / (5 * 60);
        stepIILastFiveMinutesRequestsCount = 0;
        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(2);
        stepResultDTO.setProcessingRate(avgRateOfLastFiveMin);
        stepResultDTO.setTime(new SimpleDateFormat("ss").format(new Date()));
        return stepResultDTO;
    }

    /**
     * average items per second for last 5 minutes step3
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepIIIAvgRatePerFiveMinutes() throws Exception {
        int avgRateOfLastFiveMin = stepIIILastFiveMinutesRequestsCount / (5 * 60);
        stepIIILastFiveMinutesRequestsCount = 0;
        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(3);
        stepResultDTO.setProcessingRate(avgRateOfLastFiveMin);
        stepResultDTO.setTime(new SimpleDateFormat("ss").format(new Date()));
        return stepResultDTO;
    }

    /**
     * average items per second for last 5 minutes step4
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepIVAvgRatePerFiveMinutes() throws Exception {
        int avgRateOfLastFiveMin = stepIVLastFiveMinutesRequestsCount / (5 * 60);
        stepIVLastFiveMinutesRequestsCount = 0;
        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(4);
        stepResultDTO.setProcessingRate(avgRateOfLastFiveMin);
        stepResultDTO.setTime(new SimpleDateFormat("ss").format(new Date()));
        return stepResultDTO;
    }

    /**
     * average items per second for last 5 minutes step5
     * @return
     * @throws Exception
     */
    @Override
    public StepResultDTO stepVAvgRatePerFiveMinutes() throws Exception {
        int avgRateOfLastFiveMin = stepVLastFiveMinutesRequestsCount / (5 * 60);
        stepVLastFiveMinutesRequestsCount = 0;
        StepResultDTO stepResultDTO = new StepResultDTO();
        stepResultDTO.setStepNumber(5);
        stepResultDTO.setProcessingRate(avgRateOfLastFiveMin);
        stepResultDTO.setTime(new SimpleDateFormat("ss").format(new Date()));
        return stepResultDTO;
    }

}
