package lk.ijse.innovationgeeks.rest.controller;

import lk.ijse.innovationgeeks.rest.dto.LineDTO;
import lk.ijse.innovationgeeks.rest.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/lines")
public class lineController {
    @Autowired
    private LineService lineService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean registerNewLine(@RequestBody LineDTO lineDTO) {
        try {
            return lineService.registerNewLine(lineDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
