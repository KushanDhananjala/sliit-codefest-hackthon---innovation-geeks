package lk.ijse.innovationgeeks.rest.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class IOTDevice {

    @Id
    private String deviceID;
    @ManyToOne
    @JoinColumn(name = "lineID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Line line;
    private String deviceName;

    public IOTDevice() {
    }

    public IOTDevice(String deviceID, Line line, String deviceName) {
        this.setDeviceID(deviceID);
        this.setLine(line);
        this.setDeviceName(deviceName);
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public String toString() {
        return "IOTDevice{" +
                "deviceID='" + deviceID + '\'' +
                ", line=" + line +
                ", deviceName='" + deviceName + '\'' +
                '}';
    }
}
