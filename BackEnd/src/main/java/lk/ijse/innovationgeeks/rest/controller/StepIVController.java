package lk.ijse.innovationgeeks.rest.controller;

import lk.ijse.innovationgeeks.rest.dto.StepDTO;
import lk.ijse.innovationgeeks.rest.dto.StepResultDTO;
import lk.ijse.innovationgeeks.rest.service.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/stepIV")
public class StepIVController {
    @Autowired
    private StepService stepService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public StepResultDTO stepIV(@RequestBody StepDTO stepDTO){
        try {
            return stepService.stepIV(stepDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public StepResultDTO stepIAvgRatePerFiveMinutes() {
        try {
            return stepService.stepIVAvgRatePerFiveMinutes();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
