package lk.ijse.innovationgeeks.rest.service.impl;

import lk.ijse.innovationgeeks.rest.repository.ManagerReposiory;
import lk.ijse.innovationgeeks.rest.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerReposiory managerReposiory;

    /**
     * Save Manager to Database
     */
    public void saveManager(){
        System.out.println(managerReposiory);
    }
}
