package lk.ijse.innovationgeeks.rest.dto;

public class StepResultDTO {

    private int stepNumber;
    private int processingRate;
    private int errorPercentage;
    private String time;

    public StepResultDTO() {
    }

    public StepResultDTO(int stepNumber, int processingRate, int errorPercentage, String time) {
        this.setStepNumber(stepNumber);
        this.setProcessingRate(processingRate);
        this.setErrorPercentage(errorPercentage);
        this.setTime(time);
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public int getProcessingRate() {
        return processingRate;
    }

    public void setProcessingRate(int processingRate) {
        this.processingRate = processingRate;
    }

    public int getErrorPercentage() {
        return errorPercentage;
    }

    public void setErrorPercentage(int errorPercentage) {
        this.errorPercentage = errorPercentage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
