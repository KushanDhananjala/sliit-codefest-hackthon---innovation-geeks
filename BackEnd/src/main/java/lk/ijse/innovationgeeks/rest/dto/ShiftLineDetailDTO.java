package lk.ijse.innovationgeeks.rest.dto;

public class ShiftLineDetailDTO {

    private String shiftID;
    private String lineID;

    public ShiftLineDetailDTO() {
    }

    public ShiftLineDetailDTO(String shiftID, String lineID) {
        this.setShiftID(shiftID);
        this.setLineID(lineID);
    }

    public String getShiftID() {
        return shiftID;
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }
}
