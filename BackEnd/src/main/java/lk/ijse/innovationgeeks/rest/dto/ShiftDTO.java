package lk.ijse.innovationgeeks.rest.dto;

import java.util.List;

public class ShiftDTO {

    private String shiftID;
    private String shiftDate;
    private String startTime;
    private String endTime;
    private String managerID;
    private List<ShiftLineDetailDTO> shiftLineDetailDTOs;
    private List<ShiftEmployeeDetailDTO> shiftEmployeeDetailDTOs;

    public ShiftDTO() {
    }

    public ShiftDTO(String shiftID, String shiftDate, String startTime, String endTime, String managerID, List<ShiftLineDetailDTO> shiftLineDetailDTOs, List<ShiftEmployeeDetailDTO> shiftEmployeeDetailDTOs) {
        this.shiftID = shiftID;
        this.shiftDate = shiftDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.managerID = managerID;
        this.shiftLineDetailDTOs = shiftLineDetailDTOs;
        this.shiftEmployeeDetailDTOs = shiftEmployeeDetailDTOs;
    }

    public String getShiftID() {
        return shiftID;
    }

    public void setShiftID(String shiftID) {
        this.shiftID = shiftID;
    }

    public String getShiftDate() {
        return shiftDate;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = shiftDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getManagerID() {
        return managerID;
    }

    public void setManagerID(String managerID) {
        this.managerID = managerID;
    }

    public List<ShiftLineDetailDTO> getShiftLineDetailDTOs() {
        return shiftLineDetailDTOs;
    }

    public void setShiftLineDetailDTOs(List<ShiftLineDetailDTO> shiftLineDetailDTOs) {
        this.shiftLineDetailDTOs = shiftLineDetailDTOs;
    }

    public List<ShiftEmployeeDetailDTO> getShiftEmployeeDetailDTOs() {
        return shiftEmployeeDetailDTOs;
    }

    public void setShiftEmployeeDetailDTOs(List<ShiftEmployeeDetailDTO> shiftEmployeeDetailDTOs) {
        this.shiftEmployeeDetailDTOs = shiftEmployeeDetailDTOs;
    }
}
