package lk.ijse.innovationgeeks.rest.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Line {

    @Id
    private String lineID;
    private String lineName;
    @OneToMany(cascade = CascadeType.ALL)
    private
    List<IOTDevice> iotDevices;

    public Line() {
    }

    public Line(String lineID, String lineName) {
        this.setLineID(lineID);
        this.setLineName(lineName);
    }

    public Line(String lineID, String lineName, List<IOTDevice> iotDevices) {
        this.lineID = lineID;
        this.lineName = lineName;
        this.setIotDevices(iotDevices);
    }

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public List<IOTDevice> getIotDevices() {
        return iotDevices;
    }

    public void setIotDevices(List<IOTDevice> iotDevices) {
        this.iotDevices = iotDevices;
    }

    @Override
    public String toString() {
        return "Line{" +
                "lineID='" + lineID + '\'' +
                ", lineName='" + lineName + '\'' +
                ", iotDevices=" + iotDevices +
                '}';
    }
}
