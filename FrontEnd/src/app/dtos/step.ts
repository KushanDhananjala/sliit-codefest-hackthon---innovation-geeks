import {Line} from "./line";
import {Iotdevice} from "./iotdevice";

export class Step {

  stepNumber: number;
  itemStatus: boolean;
  lineDTO: Line = new Line();
  iotDeviceDTO: Iotdevice = new Iotdevice();

}
