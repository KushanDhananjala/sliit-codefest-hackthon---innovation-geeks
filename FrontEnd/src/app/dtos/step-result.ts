export class StepResult {

  stepNumberz: number;
  processingRate: number;
  errorPercentage: number;
  time: string;

}
