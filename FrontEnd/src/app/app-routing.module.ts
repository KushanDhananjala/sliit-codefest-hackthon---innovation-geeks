import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./views/dashboard/dashboard.component";
import {ReportsComponent} from "./views/reports/reports.component";


const appRoutes: Routes = [

  {
    path: "dashboard",
    component: DashboardComponent
  },
  {
    path: "report",
    component: ReportsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {
}
