import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {NavigationComponent} from "./views/shared/header-navigation/navigation.component";
import {SidebarComponent} from "./views/shared/sidebar/sidebar.component";
import {AppRoutingModule} from "./app-routing.module";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {ReportsComponent} from './views/reports/reports.component';
import {Step} from "./dtos/step";
import {StepService} from "./services/step.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    SidebarComponent,
    DashboardComponent,
    ReportsComponent
  ],
  imports: [
    RouterModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule
  ],
  providers: [
    StepService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
