import {Component, OnInit} from '@angular/core';
import {Step} from "../../dtos/step";
import {StepResult} from "../../dtos/step-result";
import {StepService} from "../../services/step.service";
import {Line} from "../../dtos/line";
import {Iotdevice} from "../../dtos/iotdevice";
import {interval, Observable} from 'rxjs';
import * as CanvasJS from '../../../assets/js/canvasjs.min';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  stepIResult: StepResult = new StepResult();
  stepIIResult: StepResult = new StepResult();
  stepIIIResult: StepResult = new StepResult();
  stepIVResult: StepResult = new StepResult();
  stepVResult: StepResult = new StepResult();

  stepStatus: Array<boolean> = [true, true, true, true, true, false];
  stepNumber: Array<number> = [1, 2, 3, 4, 5];
  iotDevicesIDs: Array<string> = ["IOT001", "IOT002", "IOT003", "IOT004", "IOT005"];

  constructor(private stepService: StepService) {
  }

  ngOnInit() {

    /**
     * call checkStep() method to send 100 http request per every 5 minutes
     */
    interval(1000).subscribe(x => {
      this.checkStep();
    });
    this.loadStepAvgRequestRateLastFiveMinutes();

  }

  checkStep(): void {
    for (let i = 0; i <= 100; i++) {
      let step: Step = new Step();
      let line: Line = new Line();
      line.lineID = "L001";
      let iotdevice: Iotdevice = new Iotdevice();
      iotdevice.deviceID = this.iotDevicesIDs[Math.floor(Math.random() * this.iotDevicesIDs.length)];
      iotdevice.lineID = line.lineID;

      step.stepNumber = this.stepNumber[Math.floor(Math.random() * this.stepNumber.length)];
      step.itemStatus = this.stepStatus[Math.floor(Math.random() * this.stepStatus.length)];
      step.lineDTO = line;
      step.iotDeviceDTO = iotdevice;

      if (step.stepNumber == 1) {
        this.saveStepI(step);
      } else if (step.stepNumber == 2) {
        this.saveStepII(step);
      } else if (step.stepNumber == 3) {
        this.saveStepIII(step);
      } else if (step.stepNumber == 4) {
        this.saveStepIV(step);
      } else if (step.stepNumber == 5) {
        this.saveStepV(step);
      }
    }
  }

  /**
   * send IOT devices stepwise data backend to process
   * @param step
   */
  saveStepI(step: Step) {
    this.stepService.saveStepI(step).subscribe(
      (result) => {
        if (result) {
          this.stepIResult = result;
        } else {

        }
      }
    );
  }

  /**
   * send IOT devices stepwise data backend to process
   * @param step
   */
  saveStepII(step: Step) {
    this.stepService.saveStepII(step).subscribe(
      (result) => {
        if (result) {
          this.stepIIResult = result;
        } else {

        }
      }
    );
  }

  /**
   * send IOT devices stepwise data backend to process
   * @param step
   */
  saveStepIII(step: Step) {
    this.stepService.saveStepIII(step).subscribe(
      (result) => {
        if (result) {
          this.stepIIIResult = result;
        } else {

        }
      }
    );
  }

  /**
   * send IOT devices stepwise data backend to process
   * @param step
   */
  saveStepIV(step: Step) {
    this.stepService.saveStepIV(step).subscribe(
      (result) => {
        if (result) {
          this.stepIVResult = result;
        } else {

        }
      }
    );
  }

  /**
   * send IOT devices stepwise data backend to process
   * @param step
   */
  saveStepV(step: Step) {
    this.stepService.saveStepV(step).subscribe(
      (result) => {
        if (result) {
          this.stepVResult = result;
        } else {

        }
      }
    );
  }

  /**
   * Initialize Live Chart
   */
  loadStepAvgRequestRateLastFiveMinutes() {
    let dataPoints = [];
    let dataPoints1 = [];
    let dataPoints2 = [];
    let dataPoints3 = [];
    let dataPoints4 = [];
    let dpsLength = 0;
    let dpsLength1 = 0;
    let dpsLength2 = 0;
    let dpsLength3 = 0;
    let dpsLength4 = 0;
    let chart = new CanvasJS.Chart("chartContainer", {
      exportEnabled: true,
      title: {
        text: "Average Items per Second for every 5 minutes"
      },
      axisX: {
        title: "Time"
      },
      axisY: {
        title: "Items"
      },
      data: [
        {
          type: "line",
          name: "Step I",
          showInLegend: true,
          dataPoints: dataPoints,
        },
        {
          type: "line",
          name: "Step II",
          showInLegend: true,
          dataPoints: dataPoints1
        },
        {
          type: "line",
          name: "Step III",
          showInLegend: true,
          dataPoints: dataPoints2
        },
        {
          type: "line",
          name: "Step IV",
          showInLegend: true,
          dataPoints: dataPoints3
        },
        {
          type: "line",
          name: "Step V",
          showInLegend: true,
          dataPoints: dataPoints4
        }
      ]
    });

    // step I
    $.getJSON("http://localhost:8080/api/v1/stepI", function (data) {

      $.each(data, function (key, value) {
        dataPoints.push({x: data.time, y: data.processingRate});
      });
      dpsLength = dataPoints.length;
      chart.render();
      updateChart();

    });

    function updateChart() {

      $.getJSON("http://localhost:8080/api/v1/stepI", function (data) {
        $.each(data, function (key, value) {
          if (data.processingRate != 0) {
            dataPoints.push({
              x: parseInt(data.time),
              y: parseInt(data.processingRate)
            });
            dpsLength++;
          }
        });

        if (dataPoints.length > 200) {
          dataPoints.shift();
        }
        chart.render();
        setTimeout(function () {
          updateChart();
          // console.log(data);
        }, 1000 * 60 * 5)
      });
    }

    // step I end

    // step II
    $.getJSON("http://localhost:8080/api/v1/stepII", function (data1) {

      $.each(data1, function (key, value) {
        dataPoints.push({x: data1.time, y: data1.processingRate});
      });
      dpsLength1 = dataPoints1.length;
      chart.render();
      updateChart1();

    });

    function updateChart1() {

      $.getJSON("http://localhost:8080/api/v1/stepII", function (data1) {
        $.each(data1, function (key, value) {
          if (data1.processingRate != 0) {
            dataPoints1.push({
              x: parseInt(data1.time),
              y: parseInt(data1.processingRate)
            });
            dpsLength1++;
          }
        });

        if (dataPoints1.length > 200) {
          dataPoints1.shift();
        }
        chart.render();
        setTimeout(function () {
          updateChart1();
        }, 1000 * 60 * 5)
      });
    }

    // step II end

    // step III
    $.getJSON("http://localhost:8080/api/v1/stepIII", function (data2) {

      $.each(data2, function (key, value) {
        dataPoints.push({x: data2.time, y: data2.processingRate});
      });
      dpsLength1 = dataPoints1.length;
      chart.render();
      updateChart2();

    });

    function updateChart2() {

      $.getJSON("http://localhost:8080/api/v1/stepIII", function (data2) {
        $.each(data2, function (key, value) {
          if (data2.processingRate != 0) {
            dataPoints2.push({
              x: parseInt(data2.time),
              y: parseInt(data2.processingRate)
            });
            dpsLength2++;
          }
        });

        if (dataPoints2.length > 200) {
          dataPoints2.shift();
        }
        chart.render();
        setTimeout(function () {
          updateChart2();
        }, 1000 * 60 * 5)
      });
      // step III end

      // step IV
      $.getJSON("http://localhost:8080/api/v1/stepIV", function (data3) {

        $.each(data3, function (key, value) {
          dataPoints3.push({x: data3.time, y: data3.processingRate});
        });
        dpsLength3 = dataPoints3.length;
        chart.render();
        updateChart3();

      });

      function updateChart3() {

        $.getJSON("http://localhost:8080/api/v1/stepIV", function (data3) {
          $.each(data3, function (key, value) {
            if (data3.processingRate != 0) {
              dataPoints3.push({
                x: parseInt(data3.time),
                y: parseInt(data3.processingRate)
              });
              dpsLength3++;
            }
          });

          if (dataPoints3.length > 200) {
            dataPoints3.shift();
          }
          chart.render();
          setTimeout(function () {
            updateChart3();
          }, 1000)
        });
      }
      // step IV end

      // step V
      $.getJSON("http://localhost:8080/api/v1/stepV", function (data4) {

        $.each(data4, function (key, value) {
          dataPoints4.push({x: data4.time, y: data4.processingRate});
        });
        dpsLength4 = dataPoints4.length;
        chart.render();
        updateChart4();

      });

      function updateChart4() {

        $.getJSON("http://localhost:8080/api/v1/stepV", function (data4) {
          $.each(data4, function (key, value) {
            if (data4.processingRate != 0) {
              dataPoints4.push({
                x: parseInt(data4.time),
                y: parseInt(data4.processingRate)
              });
              dpsLength4++;
            }
          });

          if (dataPoints4.length > 200) {
            dataPoints4.shift();
          }
          chart.render();
          setTimeout(function () {
            updateChart4();
          }, 1000 * 60 * 5)
        });
      }
      // step V end

    }
  }

}
