import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Step} from "../dtos/step";
import {Observable} from "rxjs";
import {StepResult} from "../dtos/step-result";

export const MAIN_URL = 'http://localhost:8080';
const URLI = '/api/v1/stepI';
const URLII = '/api/v1/stepII';
const URLIII = '/api/v1/stepIII';
const URLIV = '/api/v1/stepIV';
const URLV = '/api/v1/stepV';

@Injectable()
export class StepService {

  constructor(private http: HttpClient) {
  }

  saveStepI(step: Step): Observable<StepResult> {
    return this.http.post<StepResult>(MAIN_URL + URLI, step);
  }

  saveStepII(step: Step): Observable<StepResult> {
    return this.http.post<StepResult>(MAIN_URL + URLII, step);
  }

  saveStepIII(step: Step): Observable<StepResult> {
    return this.http.post<StepResult>(MAIN_URL + URLIII, step);
  }

  saveStepIV(step: Step): Observable<StepResult> {
    return this.http.post<StepResult>(MAIN_URL + URLIV, step);
  }

  saveStepV(step: Step): Observable<StepResult> {
    return this.http.post<StepResult>(MAIN_URL + URLV, step);
  }

  getStepIAvgRequestRateLastFiveMinutes(): Observable<number> {
    return this.http.get<number>(MAIN_URL + URLI);
  }

  getStepIIAvgRequestRateLastFiveMinutes(): Observable<number> {
    return this.http.get<number>(MAIN_URL + URLII);
  }

  getStepIIIAvgRequestRateLastFiveMinutes(): Observable<number> {
    return this.http.get<number>(MAIN_URL + URLIII);
  }

  getStepIVAvgRequestRateLastFiveMinutes(): Observable<number> {
    return this.http.get<number>(MAIN_URL + URLIV);
  }

  getStepVAvgRequestRateLastFiveMinutes(): Observable<number> {
    return this.http.get<number>(MAIN_URL + URLV);
  }

}
