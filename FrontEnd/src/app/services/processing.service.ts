import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProcessingRate} from "../dto/processingRate";

@Injectable({
  providedIn: 'root'
})
export class ProcessingService {

  constructor(private http:HttpClient) { }

  process():Observable<ProcessingRate>{
    return this.http.get<ProcessingRate>("http://localhost:8080/send/message");
  }

}
